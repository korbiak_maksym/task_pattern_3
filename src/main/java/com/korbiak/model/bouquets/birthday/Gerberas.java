package com.korbiak.model.bouquets.birthday;

import com.korbiak.model.User;
import com.korbiak.model.bouquets.Bouquet;

import java.util.ArrayList;
import java.util.List;

public class Gerberas implements Bouquet {

    int price;

    List<String> comp;

    public Gerberas() {
        price = 500;
    }

    @Override
    public void writeOffs(User user) {

        user.setMoney(user.getMoney() - (price - user.getCard().getEffect()));
    }

    @Override
    public void prepare() {
        comp = new ArrayList<>();
        comp.add("25 gerberas");
        comp.add("tapes");
    }

    @Override
    public List<String> getToppings() {
        return comp;
    }

    @Override
    public void box() {
        comp.add("box");
    }

    @Override
    public String toString() {
        return "Gerberas{" +
                "price=" + price +
                ", comp=" + comp +
                '}';
    }
}
