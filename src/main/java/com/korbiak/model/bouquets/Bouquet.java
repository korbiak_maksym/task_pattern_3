package com.korbiak.model.bouquets;

import com.korbiak.model.User;

import java.util.List;

public interface Bouquet {

    void writeOffs(User user);

    void prepare();

    void box();

    List<String> getToppings();
}
