package com.korbiak.model;

import com.korbiak.model.bouquets.Bouquet;
import com.korbiak.model.decorator.BouquetsDecorator;
import com.korbiak.model.decorator.Delivery;
import com.korbiak.model.decorator.PresentBox;
import com.korbiak.model.factory.BirthdayFactory;
import com.korbiak.model.factory.FuneralFactory;
import com.korbiak.model.factory.Salon;
import com.korbiak.model.factory.WeddingSalon;
import com.korbiak.model.card.SilverCard;

public class Model {
    User user;
    Bouquet bouquet;

    public Model() {
        user = new User("Maksym Korbiak", 2500, new SilverCard());
        bouquet = null;
    }

    public String getUserInfo() {
        return String.valueOf(user);
    }

    public void initUser(String name, int money) {
        user = new User(name, money);
    }

    public void createBouquet(int fIndex, int bIndex) {
        Salon salon = null;
        if (fIndex == 1) {
            salon = new WeddingSalon();
            if (bIndex == 1) bouquet = salon.assemble(Enum.RR, user);
            if (bIndex == 2) bouquet = salon.assemble(Enum.CS, user);
        } else if (fIndex == 2) {
            salon = new BirthdayFactory();
            if (bIndex == 1) bouquet = salon.assemble(Enum.G, user);
        } else if (fIndex == 3) {
            salon = new FuneralFactory();
            if (bIndex == 1) bouquet = salon.assemble(Enum.C, user);
        }
    }

    public void addComp() {
        BouquetsDecorator delivery = new Delivery(user);
        BouquetsDecorator presentBox = new PresentBox(user);
        delivery.setBouquet(bouquet);
        presentBox.setBouquet(delivery);

        bouquet = presentBox;

    }

    public String bouquetInfo() {
        if (bouquet == null) return "Null bouquet";
        return String.valueOf(bouquet);
    }
}
