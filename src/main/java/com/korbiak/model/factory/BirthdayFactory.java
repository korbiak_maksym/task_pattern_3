package com.korbiak.model.factory;

import com.korbiak.model.Enum;
import com.korbiak.model.bouquets.Bouquet;
import com.korbiak.model.bouquets.birthday.Gerberas;
import com.korbiak.model.bouquets.funeral.Сhrysanthemum;

public class BirthdayFactory extends Salon {


    @Override
    protected Bouquet create(Enum model) {
        Bouquet bouquet = null;

        if (model == Enum.G) {
            bouquet = new Gerberas();
        }

        return bouquet;
    }
}
