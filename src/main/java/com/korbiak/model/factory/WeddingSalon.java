package com.korbiak.model.factory;

import com.korbiak.model.Enum;
import com.korbiak.model.bouquets.Bouquet;
import com.korbiak.model.bouquets.wedding.RedRose;
import com.korbiak.model.bouquets.wedding.СascadingBouquet;

public class WeddingSalon extends Salon {

    @Override
    protected Bouquet create(Enum model) {
        Bouquet bouquet = null;

        if (model == Enum.RR) {
            bouquet = new RedRose();
        } if (model == Enum.CS){
            bouquet = new СascadingBouquet();
        }

        return bouquet;
    }
}
