package com.korbiak.model.factory;

import com.korbiak.model.Enum;
import com.korbiak.model.User;
import com.korbiak.model.bouquets.Bouquet;

public abstract class Salon {
    protected abstract Bouquet create(Enum model);

    public Bouquet assemble(Enum model, User user) {
        Bouquet bouquet = create(model);
        bouquet.writeOffs(user);
        bouquet.prepare();
        bouquet.box();
        return bouquet;
    }
}
