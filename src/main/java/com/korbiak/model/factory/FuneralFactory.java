package com.korbiak.model.factory;

import com.korbiak.model.Enum;
import com.korbiak.model.bouquets.Bouquet;
import com.korbiak.model.bouquets.funeral.Сhrysanthemum;

public class FuneralFactory extends Salon {
    @Override
    protected Bouquet create(Enum model) {
        Bouquet bouquet = null;

        if (model == Enum.C) {
            bouquet = new Сhrysanthemum();
        }

        return bouquet;
    }
}
