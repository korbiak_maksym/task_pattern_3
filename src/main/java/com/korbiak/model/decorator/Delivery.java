package com.korbiak.model.decorator;

import com.korbiak.model.User;

public class Delivery extends BouquetsDecorator {

  private final int ADDITIONAL_PRICE = 30;
  private final String TO_NAME = " + Delivery";

  public Delivery(User user) {
    setAdditionalPrice(ADDITIONAL_PRICE);
    setToName(TO_NAME);
    user.setMoney(user.getMoney() - ADDITIONAL_PRICE);
  }

    @Override
    public String toString() {
      return "PresentBox{" +
              "ADDITIONAL_PRICE=" + ADDITIONAL_PRICE +
              ", TO_NAME='" + TO_NAME + '\'' +
              "}\n" + getBouquet();
    }
  }
