package com.korbiak.model.decorator;

import com.korbiak.model.User;
import com.korbiak.model.bouquets.Bouquet;

import java.util.List;
import java.util.Optional;

public class BouquetsDecorator implements Bouquet {

    private Optional<Bouquet> bouquet;
    private int additionalPrice;
    private String toName = "";
    private String additionalComponent;

    public void setBouquet(Bouquet outBo) {
        bouquet = Optional.ofNullable(outBo);
        if (additionalComponent != null) {
            bouquet.orElseThrow(IllegalArgumentException::new).getToppings().add(additionalComponent);
        }

    }

    public Optional<Bouquet> getBouquet() {
        return bouquet;
    }

    public void setBouquet(Optional<Bouquet> bouquet) {
        this.bouquet = bouquet;
    }

    public double getAdditionalPrice() {
        return additionalPrice;
    }

    public String getToName() {
        return toName;
    }

    public String getAdditionalComponent() {
        return additionalComponent;
    }

    public void setAdditionalComponent(String additionalComponent) {
        this.additionalComponent = additionalComponent;
    }

    protected void setAdditionalPrice(int additionalPrice) {

        this.additionalPrice = additionalPrice;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public List<String> getToppings() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getToppings();
    }

    @Override
    public void writeOffs(User user) {

    }

    @Override
    public void prepare() {

    }

    @Override
    public void box() {

    }

    @Override
    public String toString() {
        return "BouquetsDecorator{" +
                "bouquet=" + bouquet +
                ", additionalPrice=" + additionalPrice +
                ", toName='" + toName + '\'' +
                ", additionalComponent='" + additionalComponent + '\'' +
                '}';
    }
}
