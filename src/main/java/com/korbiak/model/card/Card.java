package com.korbiak.model.card;

public interface Card {
    int getEffect();
}
