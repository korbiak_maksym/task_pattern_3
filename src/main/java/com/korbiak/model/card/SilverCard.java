package com.korbiak.model.card;

public class SilverCard implements Card {
    int discount;

    public SilverCard() {
        discount = 100;
    }

    @Override
    public int getEffect() {
        return discount;
    }

    @Override
    public String toString() {
        return "SilverCard{" +
                "discount=" + discount +
                '}';
    }
}
